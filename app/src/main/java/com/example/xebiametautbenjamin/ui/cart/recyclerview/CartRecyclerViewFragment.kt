package com.example.xebiametautbenjamin.ui.cart.recyclerview

import android.transition.TransitionInflater
import android.view.View
import android.view.ViewTreeObserver
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.databinding.FragmentCartRecyclerviewBinding
import com.example.xebiametautbenjamin.ui.base.BaseFragment
import com.example.xebiametautbenjamin.utils.injector
import kotlinx.android.synthetic.main.fragment_books_recyclerview.*

class CartRecyclerViewFragment : BaseFragment<CartRecyclerViewViewModel, FragmentCartRecyclerviewBinding>(
R.layout.fragment_cart_recyclerview,
CartRecyclerViewViewModel::class.java), AdapterOnClickCart {

    private var cartRecyclerViewAdapter = CartRecyclerViewAdapter(this)
    override fun inject() {
        injector.inject(this)
    }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.adapter = cartRecyclerViewAdapter
    }

    override fun initUi() {
        books_recycler_view.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = cartRecyclerViewAdapter
            it.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    startPostponedEnterTransition()
                    it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }



    }

    override fun initObservers() {
        viewModel.cartBooks.observe(viewLifecycleOwner, Observer {
                cartBooks ->

            cartBooks?.let { cartRecyclerViewAdapter.submitList(it) }
        })


    }

    override fun initTransitions() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }

    override fun onMoreClick(book: BookEntity) {
        viewModel.incrementCount(book.isbn)
    }

    override fun onLessClick(book: BookEntity) {
        viewModel.decrementCount(book.isbn)
   }



}
