package com.example.xebiametautbenjamin.ui.checkout.fragment

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.ViewTreeObserver
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.databinding.FragmentCartRecyclerviewBinding
import com.example.xebiametautbenjamin.databinding.FragmentCheckoutBinding
import com.example.xebiametautbenjamin.ui.base.BaseFragment
import com.example.xebiametautbenjamin.ui.cart.recyclerview.AdapterOnClickCart
import com.example.xebiametautbenjamin.ui.cart.recyclerview.CartRecyclerViewAdapter
import com.example.xebiametautbenjamin.ui.cart.recyclerview.CartRecyclerViewViewModel
import com.example.xebiametautbenjamin.utils.injector
import kotlinx.android.synthetic.main.fragment_books_recyclerview.*
import kotlinx.android.synthetic.main.fragment_checkout.*
import timber.log.Timber

class CheckoutFragment : BaseFragment<CheckoutViewModel, FragmentCheckoutBinding>(
    R.layout.fragment_checkout,
    CheckoutViewModel::class.java) {

    private var receipeRecyclerViewAdapter = ReceipeRecyclerViewAdapter()


    override fun inject() {
        injector.inject(this)
    }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.adapter = receipeRecyclerViewAdapter
        binding.lifecycleOwner = this


    }

    override fun initUi() {
        receipe_recyclerview.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    startPostponedEnterTransition()
                    it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })

        }

    }

    override fun initObservers() {
        viewModel.cartBooks.observe(viewLifecycleOwner, Observer {
                cartBooks ->

            cartBooks?.let { receipeRecyclerViewAdapter.submitList(it) }

            cartBooks?.let {

                cartBooks.forEach{

                    viewModel.totalValue.value = viewModel.totalValue.value?.plus(it.price*it.count)

                }
            }

            if(cartBooks != null){
                viewModel.initDataOffers()
                viewModel.checkBestOfferToApply()
            }



        })

        viewModel._dataOffers.observe(viewLifecycleOwner, Observer{
            Timber.d("DATA OFFERS WAS INIT")
            viewModel.initOffers()
        })

        viewModel.offers.observe(viewLifecycleOwner, Observer {
            Timber.d("OFFERS WAS INIT")
            viewModel.checkBestOfferToApply()
        })


    }

    override fun initTransitions() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }

}