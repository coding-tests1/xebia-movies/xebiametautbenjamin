package com.example.xebiametautbenjamin.ui.checkout.fragment

import androidx.lifecycle.*
import com.example.xebiametautbenjamin.data.Data
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.OfferEntity
import com.example.xebiametautbenjamin.data.repositories.BooksRepository
import com.example.xebiametautbenjamin.utils.addMultipleTime
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class CheckoutViewModel @Inject constructor(private val booksRepository: BooksRepository) : ViewModel(){
    private val _data: LiveData<Data<List<BookEntity>>> = booksRepository.getBooksOnlyFromDbToCart()


    val cartBooks: LiveData<List<BookEntity>?> = Transformations.map(_data) {it.data}

    var _dataOffers: MutableLiveData<Data<List<OfferEntity>>> = MutableLiveData()

    var offers: MutableLiveData<List<OfferEntity>?> = MutableLiveData()
    var totalValue: MutableLiveData<Int> = MutableLiveData(0)
    var finalTotalValue: MutableLiveData<Int> = MutableLiveData(0)


    var valueOffer: MutableLiveData<String> = MutableLiveData()

    val loading: LiveData<Boolean> = Transformations.map(_data) {it.status == Data.Status.LOADING}
    val error: LiveData<String?> = Transformations.map(_data) {it.error?.localizedMessage}


    fun initDataOffers(){

         val isbns : ArrayList<String> = ArrayList(0)

        cartBooks.value?.forEach {


            isbns.addMultipleTime(it.isbn, it.count)
        }

        Timber.d(isbns.toString())

        _dataOffers = booksRepository.getOffersForCurrentCart(isbns)

        Timber.d(_dataOffers.value!!.status.toString())
    }


    fun initOffers(){
        val mediator = MediatorLiveData<List<OfferEntity>>()

        mediator.addSource(_dataOffers) {
            offers.value = it.data
        }
    }


    fun checkBestOfferToApply(){

        val totalOption : ArrayList<Int> = ArrayList(0)
        totalOption.add(0)

        totalValue.value?.let {

            offers.value?.forEach { offer ->

                when (offer.type) {
                    "percentage" -> {
                        totalOption.add((offer.value * 100)/ totalValue.value!!)
                    }
                    "minus" -> {
                        totalOption.add(totalValue.value!! - offer.value)
                    }
                    "slice" -> {
                        totalOption.add(totalValue.value!! - ((totalValue.value!! / offer.sliceValue!!) * offer.value))
                    }
                }
            }
        }


        valueOffer.value = Collections.max(totalOption).toString()



        finalTotalValue.value = totalValue.value!!.toInt() - valueOffer.value!!.toInt()





    }
}