package com.example.xebiametautbenjamin.ui.cart.recyclerview

import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity

interface AdapterOnClickCart {

    fun onMoreClick(book: BookEntity)

    fun onLessClick(book: BookEntity)
}