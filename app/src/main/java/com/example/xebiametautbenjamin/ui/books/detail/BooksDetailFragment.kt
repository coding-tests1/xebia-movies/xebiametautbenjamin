package com.example.xebiametautbenjamin.ui.books.detail

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.databinding.FragmentBooksDetailBinding
import com.example.xebiametautbenjamin.ui.base.BaseFragment
import com.example.xebiametautbenjamin.utils.injector
import kotlinx.android.synthetic.main.fragment_books_detail.*
import timber.log.Timber

class BooksDetailFragment : BaseFragment<BooksDetailViewModel, FragmentBooksDetailBinding>
    (R.layout.fragment_books_detail, BooksDetailViewModel::class.java){

    private val args: BooksDetailFragmentArgs by navArgs()

    private val requestListener = object: RequestListener<Drawable>{
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {

            startPostponedEnterTransition()
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {

            startPostponedEnterTransition()
            return false
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setBookIsbn(args.bookIsbn)




    }

    override fun initUi() {

        add_to_cart_button.setOnClickListener {
            viewModel.incrementCount()
        }
    }



    override fun inject() {
        injector.inject(this)

    }

    override fun initObservers() {
        viewModel.coverPath.observe(viewLifecycleOwner, Observer {
            it?.let {
                    coverPath ->  loadImage(coverPath)}
        })



        viewModel.concatSynopsis.observe(viewLifecycleOwner, Observer {
            it?.let {
                Timber.d(it.toString())
            }
        })
        viewModel.count.observe(viewLifecycleOwner, Observer {count ->


        })



    }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.booksDetailCover.transitionName = args.bookIsbn + "detail"

    }

    override fun initTransitions() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }



    private fun loadImage(imageUrl: String){


        Glide.with(this)
            .load(imageUrl)
            .listener(requestListener).submit()

    }


}