package com.example.xebiametautbenjamin.ui.books.recyclerview

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.xebiametautbenjamin.data.Data
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.repositories.BooksRepository
import javax.inject.Inject

class BooksRecyclerViewViewModel @Inject constructor(private val booksRepository: BooksRepository) : ViewModel(){
    private val _data: LiveData<Data<List<BookEntity>>> = booksRepository.getBooks()

    val books: LiveData<List<BookEntity>?> = Transformations.map(_data) {it.data}
    val loading: LiveData<Boolean> = Transformations.map(_data) {it.status == Data.Status.LOADING}
    val error: LiveData<String?> = Transformations.map(_data) {it.error?.localizedMessage}


    fun incrementCount(_isbn : String){
        booksRepository.incrementCountIntoDb(_isbn)
    }


}