package com.example.xebiametautbenjamin.ui.books

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.di.components.ApplicationComponent
import com.example.xebiametautbenjamin.ui.base.BaseActivity
import com.example.xebiametautbenjamin.ui.cart.CartActivity
import kotlinx.android.synthetic.main.activity_books.*


class BooksActivity : BaseActivity(R.layout.activity_books){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(findViewById(R.id.toolbar))

        cart_button.setOnClickListener {
            intent = Intent(this, CartActivity::class.java)
            startActivity(intent)

        }

    }



}