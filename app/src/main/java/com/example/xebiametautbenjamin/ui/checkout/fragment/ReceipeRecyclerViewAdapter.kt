package com.example.xebiametautbenjamin.ui.checkout.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.databinding.FragmentCartItemBinding
import com.example.xebiametautbenjamin.databinding.FragmentCheckoutBinding
import com.example.xebiametautbenjamin.databinding.FragmentCheckoutItemBinding
import kotlinx.android.extensions.LayoutContainer

class ReceipeRecyclerViewAdapter : ListAdapter<BookEntity, ReceipeRecyclerViewAdapter.ViewHolder>
    (ReceipeRecyclerViewAdapter.DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentCheckoutItemBinding>(
            LayoutInflater.from(parent.context)
            , R.layout.fragment_checkout_item, parent, false
        )

        return ViewHolder(binding.root, binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { cartBook ->

            holder.itemView.tag = cartBook

            holder.bind(cartBook)

        }
    }


    class ViewHolder(override val containerView: View, private val binding: FragmentCheckoutItemBinding) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(book: BookEntity) {
            binding.book = book

        }

    }

    class DiffCallback : DiffUtil.ItemCallback<BookEntity>() {
        override fun areItemsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem.isbn == newItem.isbn
        }

        override fun areContentsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem == newItem
        }
    }
}