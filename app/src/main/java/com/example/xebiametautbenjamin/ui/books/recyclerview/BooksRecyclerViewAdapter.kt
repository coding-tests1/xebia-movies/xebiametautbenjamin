package com.example.xebiametautbenjamin.ui.books.recyclerview

import android.view.LayoutInflater
import androidx.core.os.bundleOf
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.xebiametautbenjamin.R

import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.databinding.FragmentBooksItemBinding

import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_cart.view.*
import kotlinx.android.synthetic.main.fragment_books_item.view.*

class BooksRecyclerViewAdapter(val adapterOnClickBuy : (BookEntity) -> Unit) : ListAdapter<BookEntity, BooksRecyclerViewAdapter.ViewHolder>
    (BooksRecyclerViewAdapter.DiffCallback()){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentBooksItemBinding>(LayoutInflater.from(parent.context)
            , R.layout.fragment_books_item, parent, false)


        return  ViewHolder(binding.root, binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { book ->

            holder.itemView.tag = book
            holder.itemView.book_item_cover.transitionName = book.isbn + "detail"
            holder.itemView.setOnClickListener {
                //holder.itemView.findNavController().navigate(R.id.booksDetailFragment)
                holder.itemView.findNavController().navigate(
                    R.id.action_booksRecyclerViewFragment_to_booksDetailFragment,
                    bundleOf("bookIsbn" to book.isbn),
                    null,
                    FragmentNavigatorExtras(holder.itemView.book_item_cover to book.isbn + "detail")
                )
            }
            holder.bind(book)

            holder.itemView.add_button.setOnClickListener{
                adapterOnClickBuy(book)
            }


        }



    }


    class ViewHolder(override val containerView: View, private val binding: FragmentBooksItemBinding)
        : RecyclerView.ViewHolder(containerView), LayoutContainer {



        fun bind(book: BookEntity) {

            binding.book = book
        }



    }

    class DiffCallback : DiffUtil.ItemCallback<BookEntity>(){
        override fun areItemsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem.isbn == newItem.isbn
        }

        override fun areContentsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem == newItem
        }
    }
}