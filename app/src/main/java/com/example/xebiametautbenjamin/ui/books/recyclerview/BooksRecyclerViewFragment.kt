package com.example.xebiametautbenjamin.ui.books.recyclerview

import android.transition.TransitionInflater
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.ui.base.BaseFragment
import com.example.xebiametautbenjamin.databinding.FragmentBooksRecyclerviewBinding
import com.example.xebiametautbenjamin.utils.injector
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.fragment_books_recyclerview.*
import timber.log.Timber

class BooksRecyclerViewFragment: BaseFragment<BooksRecyclerViewViewModel, FragmentBooksRecyclerviewBinding>(
    R.layout.fragment_books_recyclerview,
    BooksRecyclerViewViewModel::class.java) {

    private val booksRecyclerViewAdapter = BooksRecyclerViewAdapter{
        book ->

        viewModel.incrementCount(book.isbn)
    }


    override fun inject() {
        injector.inject(this)

     }

    override fun initBinding() {
        binding.viewModel = viewModel
        binding.adapter = booksRecyclerViewAdapter
    }

    override fun initUi() {
        books_recycler_view.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = booksRecyclerViewAdapter
            it.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    startPostponedEnterTransition()
                    it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })

        }


    }

    override fun initObservers() {
        viewModel.books.observe(viewLifecycleOwner, Observer {
           books ->



            books?.let { booksRecyclerViewAdapter.submitList(it) }
        })
    }

    override fun initTransitions() {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        postponeEnterTransition()
    }


}