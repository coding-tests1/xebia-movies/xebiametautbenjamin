package com.example.xebiametautbenjamin.ui.cart.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

import androidx.recyclerview.widget.RecyclerView
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.databinding.FragmentCartItemBinding
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_books_item.view.*
import kotlinx.android.synthetic.main.fragment_books_item.view.book_item_cover
import kotlinx.android.synthetic.main.fragment_cart_item.view.*

class CartRecyclerViewAdapter(val adapterOnClickCart : AdapterOnClickCart) : ListAdapter<BookEntity, CartRecyclerViewAdapter.ViewHolder>
    (CartRecyclerViewAdapter.DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<FragmentCartItemBinding>(
            LayoutInflater.from(parent.context)
            , R.layout.fragment_cart_item, parent, false
        )

        return ViewHolder(binding.root, binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { cartBook ->

            holder.itemView.tag = cartBook
            holder.itemView.book_item_cover.transitionName = cartBook.isbn + "detail"
            holder.itemView.setOnClickListener {
                //holder.itemView.findNavController().navigate(R.id.booksDetailFragment)
                holder.itemView.findNavController().navigate(
                    R.id.action_cartRecyclerViewFragment_to_booksDetailFragment2,
                    bundleOf("bookIsbn" to cartBook.isbn),
                    null,
                    FragmentNavigatorExtras(holder.itemView.book_item_cover to cartBook.isbn + "detail")
                )
            }
            holder.bind(cartBook)

            holder.itemView.more_button.setOnClickListener{

                adapterOnClickCart.onMoreClick(cartBook)
            }

            holder.itemView.less_button.setOnClickListener{
                adapterOnClickCart.onLessClick(cartBook)
            }


        }
    }


    class ViewHolder(override val containerView: View, private val binding: FragmentCartItemBinding) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(book: BookEntity) {
            binding.book = book
            //binding.book =
        }

    }

    class DiffCallback : DiffUtil.ItemCallback<BookEntity>() {
        override fun areItemsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem.isbn == newItem.isbn
        }

        override fun areContentsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
            return oldItem == newItem
        }
    }
}