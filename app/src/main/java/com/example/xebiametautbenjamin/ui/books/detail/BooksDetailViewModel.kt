package com.example.xebiametautbenjamin.ui.books.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.xebiametautbenjamin.data.Data
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity
import com.example.xebiametautbenjamin.data.repositories.BooksRepository
import com.example.xebiametautbenjamin.utils.prettyToString
import javax.inject.Inject

class BooksDetailViewModel @Inject constructor(private val booksRepository: BooksRepository) : ViewModel(){
    private val _isbn: MutableLiveData<String> = MutableLiveData()
    private val _dataBook: LiveData<Data<BookEntity>> = Transformations.switchMap(_isbn) { booksRepository.getBook(it) }
     val _dataSynopsis: LiveData<Data<List<SynopsisEntity>>> = Transformations.switchMap(_isbn) { booksRepository.getSynopsisByIsbn(it)}



    val title: LiveData<String> = Transformations.map(_dataBook) { it.data?.title }
    val coverPath: LiveData<String> = Transformations.map(_dataBook) { it.data?.cover }
    val synopsis: LiveData<List<String>> = Transformations.map(_dataSynopsis) {it.data?.map { synopsis ->  synopsis.content }}
    val concatSynopsis: LiveData<String> = Transformations.map(_dataSynopsis) { it.data?.map { synopsisEntity -> synopsisEntity.content }
        ?.prettyToString() }

    private val _count: LiveData<Int> = Transformations.map(_dataBook) { it.data?.count}

    var count: MutableLiveData<Int> = _count as MutableLiveData<Int>



    val loading: LiveData<Boolean> = Transformations.map(_dataBook) { it.status == Data.Status.LOADING}
    val error:LiveData<String?> = Transformations.map(_dataBook) { it.error?.localizedMessage}


    fun setBookIsbn(isbn: String){
        _isbn.value = isbn
    }


    fun incrementCount(){
        booksRepository.incrementCountIntoDb(_isbn.value!!)
    }



}