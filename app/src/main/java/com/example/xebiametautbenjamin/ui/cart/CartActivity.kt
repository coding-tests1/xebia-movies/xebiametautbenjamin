package com.example.xebiametautbenjamin.ui.cart


import android.content.Intent
import android.os.Bundle
import com.example.xebiametautbenjamin.R
import com.example.xebiametautbenjamin.ui.base.BaseActivity
import com.example.xebiametautbenjamin.ui.checkout.CheckoutActivity
import kotlinx.android.synthetic.main.activity_books.*
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : BaseActivity(R.layout.activity_cart){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        button_buy.setOnClickListener {
            intent = Intent(this, CheckoutActivity::class.java)
            startActivity(intent)

        }

    }


}