package com.example.xebiametautbenjamin.utils

import android.view.View
import android.widget.ImageView
import android.widget.ListAdapter
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import com.bumptech.glide.Glide

@BindingConversion
fun convertBooleanToVisibility(isVisible: Boolean) : Int{
    return if(isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun bindImageFromSrcUrl(view: ImageView, url: String?){
    url?.also{
        Glide.with(view.context)
            .load(url)
            .preload()

        Glide.with(view.context)
            .load(url)
            .into(view)
    }
}


@BindingAdapter("error")
fun bindLayoutError(view: ConstraintLayout, error: String?){
    error?.let{
        Toast.makeText(view.context, it, Toast.LENGTH_LONG).show()
    }
}

