package com.example.xebiametautbenjamin.data.local.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity


@Dao
interface SynopsisDao :  BaseDao<SynopsisEntity> {

    @Query("SELECT * FROM synopsis")
    fun getAll(): LiveData<List<SynopsisEntity>>

    @Query("SELECT * FROM synopsis WHERE isbn = :bookIsbn")
    fun getByIsbn(bookIsbn : String): LiveData<List<SynopsisEntity>>

}