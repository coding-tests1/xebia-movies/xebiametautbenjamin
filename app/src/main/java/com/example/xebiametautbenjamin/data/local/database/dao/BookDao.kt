package com.example.xebiametautbenjamin.data.local.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity

@Dao
interface BookDao : BaseDao<BookEntity> {

    @Query("SELECT * FROM books")
    fun getAll(): LiveData<List<BookEntity>>

    @Query("SELECT * FROM books WHERE isbn = :bookIsbn")
    fun getOne(bookIsbn : String): LiveData<BookEntity>

    @Query("UPDATE books Set count = count + 1 WHERE isbn = :bookIsbn")
    fun incrementCount(bookIsbn: String)

    @Query("UPDATE books Set count = count - 1 WHERE isbn = :bookIsbn")
    fun decrementCount(bookIsbn: String)



}