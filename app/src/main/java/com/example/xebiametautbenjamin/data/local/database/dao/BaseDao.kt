package com.example.xebiametautbenjamin.data.local.database.dao

import androidx.room.*
import androidx.annotation.WorkerThread


@Dao
interface BaseDao<in T> {
    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(entity: T?)

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMultiple(vararg entity: T)

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(entities: List<T>?)


    @WorkerThread
    @Update
    fun update(entity: T)

    @WorkerThread
    @Update
    fun updateMultiple(vararg entity: T)

    @WorkerThread
    @Update
    fun updateAll(entites: List<T>)

    @WorkerThread
    @Delete
    fun delete(entity: T)

    @WorkerThread
    @Delete
    fun deleteMultiple(vararg entity: T)

    @WorkerThread
    @Delete
    fun deleteAll(entities: List<T>)




}