package com.example.xebiametautbenjamin.data

import timber.log.Timber

class Data<T> private constructor(var status: Status,
                                  val data: T?,
                                  var error: Throwable?) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object{
        fun <T> success(data: T?): Data<T> = Data(Status.SUCCESS, data, null)
        fun <T> loading(): Data<T> = Data(Status.LOADING, null, null)
        fun <T> error(throwable: Throwable?): Data<T> {
            Timber.tag("ResourceError").w(throwable, "Error while getting resource: ${throwable?.localizedMessage}")
            return Data(Status.ERROR, null, throwable)
        }
    }

}