package com.example.xebiametautbenjamin.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "synopsis")
data class SynopsisEntity (


        @ColumnInfo(name = "isbn")
        val isbn: String,

        @PrimaryKey
        @ColumnInfo(name = "content")
        val content: String
)
