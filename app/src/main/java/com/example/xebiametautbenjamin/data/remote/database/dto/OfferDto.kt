package com.example.xebiametautbenjamin.data.remote.database.dto

import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.OfferEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import timber.log.Timber

@JsonClass(generateAdapter = true)
data class OfferDto (

    @Json(name = "offers")
    val offers: List<OfferContentDto>

    ) {

    fun toEntities(): List<OfferEntity> {
        return offers.map { it.toEntity() }
    }


}
