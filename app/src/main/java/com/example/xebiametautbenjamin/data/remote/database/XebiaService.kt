package com.example.xebiametautbenjamin.data.remote.database

import androidx.lifecycle.LiveData
import com.example.xebiametautbenjamin.data.remote.database.dto.BookDto
import com.example.xebiametautbenjamin.data.remote.database.dto.OfferContentDto
import com.example.xebiametautbenjamin.data.remote.database.dto.OfferDto
import com.github.leonardoxh.livedatacalladapter.Resource
import retrofit2.http.GET
import retrofit2.http.Path


interface XebiaService {
    @GET("books")
    fun getAllBooks(): LiveData<Resource<List<BookDto>>>

    @GET("books/{isbns}/commercialOffers")
    fun getOffersByBooks(@Path("isbns")  isbns : String): LiveData<Resource<OfferDto>>


}

/* private interface API {
        @GET("/thing")
        void getMyThing(@QueryMap Map<String, String>, new Callback<String> callback);
    }*/