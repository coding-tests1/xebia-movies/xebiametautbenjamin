package com.example.xebiametautbenjamin.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.example.xebiametautbenjamin.data.Data
import com.example.xebiametautbenjamin.data.local.database.dao.BookDao
import com.example.xebiametautbenjamin.data.local.database.dao.OfferDao
import com.example.xebiametautbenjamin.data.local.database.dao.SynopsisDao
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.OfferEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity
import com.example.xebiametautbenjamin.data.remote.database.XebiaService
import com.example.xebiametautbenjamin.data.remote.database.dto.BookDto
import com.example.xebiametautbenjamin.data.remote.database.dto.OfferDto
import com.github.leonardoxh.livedatacalladapter.Resource
import timber.log.Timber
import javax.inject.Inject


class BooksRepository @Inject constructor(private val xebiaService : XebiaService,
                                          private val bookDao: BookDao,
                                          private val synopsisDao: SynopsisDao,
                                          private val offerDao: OfferDao){

    fun getBooks(): LiveData<Data<List<BookEntity>>>{
        val mediator: MediatorLiveData<Data<List<BookEntity>>> = MediatorLiveData()
        mediator.value = Data.loading()


        mediator.addSource(getBooksFromDb()){
            mediator.value = Data.success(it)
        }

        mediator.addSource(getBooksFromApi()){
            when(it?.isSuccess){
                true -> {
                    bookDao.insertAll(it.resource?.map { bookDto ->
                        bookDto.toEntity()
                    })

                    //Setup all synopsis into local db
                    it.resource?.map { bookDto ->


                        bookDto.synopsis.map { content ->
                            synopsisDao.insert(bookDto.synopsisToEntity(bookDto.isbn, content))
                        }

                    }


                }
                false -> mediator.value = Data.error(it.error)
            }
        }




        return mediator
    }

    fun getBooksOnlyFromDbToCart(): LiveData<Data<List<BookEntity>>>{
        val mediator: MediatorLiveData<Data<List<BookEntity>>> = MediatorLiveData()
        mediator.value = Data.loading()

        mediator.addSource(getBooksFromDb()){

            mediator.value = Data.success(it.filter { it.count > 0 })
        }

        return mediator
    }

    fun getBook(bookIsbn : String): LiveData<Data<BookEntity>> {
        val mediator: MediatorLiveData<Data<BookEntity>> = MediatorLiveData()
        mediator.value = Data.loading()

        mediator.addSource(getBookFromDb(bookIsbn)){
            mediator.value = Data.success(it)
        }
        return mediator
    }


    fun getOffersForCurrentCart(isbns: List<String>) : MutableLiveData<Data<List<OfferEntity>>>{

        val mediator: MediatorLiveData<Data<List<OfferEntity>>> = MediatorLiveData()
        mediator.value = Data.loading()


        val isbnsToSend = isbns.joinToString(",")

        mediator.addSource(getOffersFromDb()){
            mediator.value = Data.success(it)
        }

        mediator.addSource(getOffersFromApi(isbnsToSend)){
            when(it?.isSuccess){
                true -> {
                    offerDao.insertAll(it.resource?.toEntities())


                }
                false -> mediator.value = Data.error(it.error)
            }
        }


        return mediator


    }


    fun getSynopsis(): LiveData<Data<List<SynopsisEntity>>>{

        val mediator: MediatorLiveData<Data<List<SynopsisEntity>>> = MediatorLiveData()
        mediator.value = Data.loading()

        mediator.addSource(getSynopsisFromDb()){
            mediator.value = Data.success(it)
        }

        mediator.addSource(getBooksFromApi()){
            when(it?.isSuccess){
                true -> {
                    bookDao.insertAll(it.resource?.map { bookDto ->
                        bookDto.toEntity()
                    })

                    //Setup all synopsis into local db
                    it.resource?.map { bookDto ->



                        synopsisDao.insertAll(bookDto.synopsis.map { content ->
                            bookDto.synopsisToEntity(bookDto.isbn, content)
                        })
                    }


                }
                false -> mediator.value = Data.error(it.error)
            }
        }

        return mediator

    }

    fun getSynopsisByIsbn(isbn: String) :LiveData<Data<List<SynopsisEntity>>>{
        val mediator: MediatorLiveData<Data<List<SynopsisEntity>>> = MediatorLiveData()
        mediator.value = Data.loading()

        mediator.addSource(getSynopsisByIsbnFromDb(isbn)){

            mediator.value = Data.success(it)
        }
        return mediator
    }

    private fun getBooksFromApi(): LiveData<Resource<List<BookDto>>> {
        return xebiaService.getAllBooks()
    }

    private fun getBookFromDb(bookIsbn: String): LiveData<BookEntity>{
        return bookDao.getOne(bookIsbn)
    }
    private fun getBooksFromDb(): LiveData<List<BookEntity>>{
        return bookDao.getAll()
    }

    private fun getSynopsisByIsbnFromDb(bookIsbn: String): LiveData<List<SynopsisEntity>>{
        return synopsisDao.getByIsbn(bookIsbn)
    }

    private fun getSynopsisFromDb(): LiveData<List<SynopsisEntity>>{
        return synopsisDao.getAll()
    }

    private fun getOffersFromApi(isbns : String) : LiveData<Resource<OfferDto>>{
        return xebiaService.getOffersByBooks(isbns)
    }


    private fun getOffersFromDb(): LiveData<List<OfferEntity>>{
        return offerDao.getAll()
    }


    fun incrementCountIntoDb(bookIsbn: String){
        bookDao.incrementCount(bookIsbn)
    }

    fun decrementCountIntoDb(bookIsbn: String){
        bookDao.decrementCount(bookIsbn)
    }
}