package com.example.xebiametautbenjamin.data.remote.database.dto

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.example.xebiametautbenjamin.data.local.database.entities.OfferEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OfferContentDto (
    @Json(name = "type")
    val type: String,

    @Json(name = "sliceValue")
    val sliceValue: Int?,

    @Json(name = "value")
    val value : Int
){
    fun toEntity() : OfferEntity{
        return OfferEntity(type, sliceValue, value)
    }


}

