package com.example.xebiametautbenjamin.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "books")
data class BookEntity (
    @PrimaryKey
    @ColumnInfo(name = "isbn")
    val isbn: String,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "price")
    val price: Int,

    @ColumnInfo(name = "cover")
    val cover: String,

    @ColumnInfo(name = "count")
    val count: Int
){
    
}