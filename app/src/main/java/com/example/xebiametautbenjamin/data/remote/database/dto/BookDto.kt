package com.example.xebiametautbenjamin.data.remote.database.dto

import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BookDto(
    @Json(name = "isbn")
    val isbn: String,

    @Json(name = "title")
    val title: String,

    @Json(name = "price")
    val price: Int,

    @Json(name = "cover")
    val cover: String,

    @Json(name = "synopsis")
    val synopsis: List<String>
) {
    fun toEntity(): BookEntity {
        return BookEntity(
            isbn,
            title,
            price,
            cover,
            0
            )
    }

    fun synopsisToEntity(isbn: String, content : String): SynopsisEntity{
        return SynopsisEntity(
            isbn,
            content
        )
    }
}

