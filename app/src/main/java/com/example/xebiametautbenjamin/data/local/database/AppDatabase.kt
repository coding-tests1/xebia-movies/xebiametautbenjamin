package com.example.xebiametautbenjamin.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.xebiametautbenjamin.data.local.database.dao.BookDao
import com.example.xebiametautbenjamin.data.local.database.dao.OfferDao
import com.example.xebiametautbenjamin.data.local.database.dao.SynopsisDao
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.OfferEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity


@Database(
    version = 1,
    entities = [
        BookEntity::class,
        SynopsisEntity::class,
        OfferEntity::class

    ]
)
abstract class AppDatabase : RoomDatabase(){
    abstract fun bookDao() : BookDao
    abstract fun synopsisDao(): SynopsisDao
    abstract fun offerDao() : OfferDao

}

