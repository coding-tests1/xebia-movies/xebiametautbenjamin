package com.example.xebiametautbenjamin.data.local.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "offers")
data class OfferEntity(

    @PrimaryKey
    @ColumnInfo(name = "type")
    val type: String,

    @ColumnInfo(name ="sliceValue")
    val sliceValue: Int?,

    @ColumnInfo(name = "value")
    val value : Int)