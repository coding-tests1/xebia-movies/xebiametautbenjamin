package com.example.xebiametautbenjamin.data.local.database.dao

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.xebiametautbenjamin.data.local.database.entities.BookEntity
import com.example.xebiametautbenjamin.data.local.database.entities.OfferEntity
import com.example.xebiametautbenjamin.data.local.database.entities.SynopsisEntity


@Dao
interface OfferDao : BaseDao<OfferEntity> {

    @Query("SELECT * FROM offers")
    fun getAll(): LiveData<List<OfferEntity>>



}