package com.example.xebiametautbenjamin.di.components

interface DaggerComponentProvider {
    val component: ApplicationComponent
}