package com.example.xebiametautbenjamin.di.modules

import com.example.xebiametautbenjamin.BuildConfig
import com.example.xebiametautbenjamin.data.remote.database.XebiaService
import com.github.leonardoxh.livedatacalladapter.LiveDataCallAdapterFactory
import com.github.leonardoxh.livedatacalladapter.LiveDataResponseBodyConverterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
object NetworkModule {

    @Reusable
    @JvmStatic
    @Provides
    internal fun provideXebiaService(retrofit: Retrofit) : XebiaService{
        return retrofit.create(XebiaService::class.java)
    }

    @Reusable
    @JvmStatic
    @Provides
    internal fun provideRetrofirInterface() : Retrofit {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addCallAdapterFactory(LiveDataCallAdapterFactory.create())
            .addConverterFactory(LiveDataResponseBodyConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

}