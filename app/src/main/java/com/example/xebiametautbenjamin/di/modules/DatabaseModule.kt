package com.example.xebiametautbenjamin.di.modules

import androidx.room.Room
import android.content.Context
import com.example.xebiametautbenjamin.BuildConfig
import com.example.xebiametautbenjamin.data.local.database.AppDatabase
import com.example.xebiametautbenjamin.data.local.database.dao.BookDao
import com.example.xebiametautbenjamin.data.local.database.dao.OfferDao
import com.example.xebiametautbenjamin.data.local.database.dao.SynopsisDao
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideAppDatabase(context: Context):
            AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, BuildConfig.DATABASE_NAME)
        .allowMainThreadQueries()
        .build()

    @Reusable
    @JvmStatic
    @Provides
    fun provideBookDao(appDatabase: AppDatabase): BookDao = appDatabase.bookDao()

    @Reusable
    @JvmStatic
    @Provides
    fun provideSynopsisDao(appDatabase: AppDatabase): SynopsisDao = appDatabase.synopsisDao()

    @Reusable
    @JvmStatic
    @Provides
    fun provideOfferDao(appDatabase: AppDatabase): OfferDao = appDatabase.offerDao()


}