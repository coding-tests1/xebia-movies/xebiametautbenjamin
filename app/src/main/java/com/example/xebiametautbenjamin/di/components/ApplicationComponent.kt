package com.example.xebiametautbenjamin.di.components

import android.content.Context
import com.example.xebiametautbenjamin.di.modules.DatabaseModule
import com.example.xebiametautbenjamin.di.modules.NetworkModule
import com.example.xebiametautbenjamin.di.modules.ViewModelsModule
import com.example.xebiametautbenjamin.ui.books.detail.BooksDetailFragment
import com.example.xebiametautbenjamin.ui.books.recyclerview.BooksRecyclerViewFragment
import com.example.xebiametautbenjamin.ui.cart.recyclerview.CartRecyclerViewFragment
import com.example.xebiametautbenjamin.ui.checkout.fragment.CheckoutFragment
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [
    DatabaseModule::class,
    NetworkModule::class,
    ViewModelsModule::class

])
interface ApplicationComponent {

    //Insert injection of fragment
    fun inject(fragment: BooksRecyclerViewFragment)
    fun inject(fragment: BooksDetailFragment)

    fun inject(fragment: CartRecyclerViewFragment)

    fun inject(fragment: CheckoutFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context) : Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun networkModule(networkModule: NetworkModule): Builder
        fun viewModelsModule(viewModelsModule: ViewModelsModule): Builder
        fun build(): ApplicationComponent
    }
}