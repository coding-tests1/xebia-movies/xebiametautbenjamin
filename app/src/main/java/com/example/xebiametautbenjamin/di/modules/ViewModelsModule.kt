package com.example.xebiametautbenjamin.di.modules

import androidx.lifecycle.ViewModel
import com.example.xebiametautbenjamin.data.repositories.BooksRepository
import com.example.xebiametautbenjamin.di.ViewModelFactory
import com.example.xebiametautbenjamin.ui.books.BooksViewModel
import com.example.xebiametautbenjamin.ui.books.detail.BooksDetailViewModel
import com.example.xebiametautbenjamin.ui.books.recyclerview.BooksRecyclerViewViewModel
import com.example.xebiametautbenjamin.ui.cart.recyclerview.CartRecyclerViewViewModel
import com.example.xebiametautbenjamin.ui.checkout.fragment.CheckoutViewModel
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.multibindings.IntoMap
import javax.inject.Provider
import kotlin.reflect.KClass

@Module
object ViewModelsModule {

    @Target(AnnotationTarget.FUNCTION)
    @Retention(AnnotationRetention.RUNTIME)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

    @Reusable
    @JvmStatic
    @Provides
    fun providesViewModelFactory(providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): ViewModelFactory = ViewModelFactory(providerMap)


    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(BooksViewModel::class)
    fun providesMoviesViewModel(): ViewModel = BooksViewModel()

    //Insert here views
    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(BooksRecyclerViewViewModel::class)
    fun providesBooksRecyclerViewViewModel(booksRepository: BooksRepository): ViewModel = BooksRecyclerViewViewModel(booksRepository)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(BooksDetailViewModel::class)
    fun providesBookDetailViewModel(booksRepository: BooksRepository): ViewModel = BooksDetailViewModel(booksRepository)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(CartRecyclerViewViewModel::class)
    fun providesCartRecyclerViewViewModel(booksRepository: BooksRepository): ViewModel = CartRecyclerViewViewModel(booksRepository)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(CheckoutViewModel::class)
    fun providesCheckoutViewModel(booksRepository: BooksRepository): ViewModel = CheckoutViewModel(booksRepository)

}
