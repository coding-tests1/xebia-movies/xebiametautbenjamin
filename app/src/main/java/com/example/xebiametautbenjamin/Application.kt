package com.example.xebiametautbenjamin

import android.app.Application
import com.example.xebiametautbenjamin.di.components.ApplicationComponent
import com.example.xebiametautbenjamin.di.components.DaggerApplicationComponent
import com.example.xebiametautbenjamin.di.components.DaggerComponentProvider
import com.example.xebiametautbenjamin.di.modules.DatabaseModule
import com.example.xebiametautbenjamin.di.modules.NetworkModule
import com.example.xebiametautbenjamin.di.modules.ViewModelsModule
import timber.log.Timber

//Extend Application class to inject all the dependancy !
class Application : Application(), DaggerComponentProvider{

    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .databaseModule(DatabaseModule)
            .networkModule(NetworkModule)
            .viewModelsModule(ViewModelsModule)
            .build()


    }

    override fun onCreate() {
        super.onCreate()
        initTimber()

    }

    private fun initTimber(){
        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }


}